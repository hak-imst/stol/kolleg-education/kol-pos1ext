package _20_21_WiSe._20210201_Exams;

import java.util.LinkedList;

public class _03_Join {
    public static void main(String[] args) {
        LinkedList<Thread> threads = new LinkedList<>();

        for (int i = 0; i < 3; i++) {
            Thread t = new Thread(new CounterJob());
            t.start();
            threads.add(t);
        }

        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println(DataShare.counter);
    }
}

class CounterJob implements Runnable{

    @Override
    public void run() {
        for (int i = 0; i < 1000000; i++) {
            DataShare.counter++;
        }
    }
    
}

class DataShare {
    public static int counter = 0;
}