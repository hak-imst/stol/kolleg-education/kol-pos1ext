package _20_21_WiSe._20201109_Concurrency;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class _02_Executor {
    public static void main(String[] args) {
        ExecutorService exec = Executors.newFixedThreadPool(5);
        //ExecutorService exec = Executors.newCachedThreadPool();

        exec.execute(new Job(10));

        Job run2 = new Job(10);

        int counter = 0;
        while (true) {
            if(counter < 10) exec.execute(run2);
            System.out.printf("%d Threads laufen gerade%n", Thread.activeCount());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            counter++;
        }
    }
}