package _20_21_WiSe._20201109_Concurrency;

public class _01_Runnable {
    public static void main(String[] args) {
        Thread t1 = new Thread(new Job(10), "Job 1");
        Job run2 = new Job(10);
        Thread t2 = new Thread(run2, "Job 2");
        t2.start();
        // Achtung! Nicht
        // t1.run();
        t1.start();
        // Darf man auch nicht!
        // t1.start();
        System.out.println("Hello World!");
    }
}

class Job implements Runnable {

    int runs;

    public Job(int runs) {
        this.runs = runs;
    }

    @Override
    public void run() {

        for (int i = 0; i < runs; i++) {
            // System.out.printf("%s: %d %n", Thread.currentThread().getName(), i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

}