package _20_21_WiSe._20201122_Concurrency_race_condition;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class _04_HashApp {
    public static void main(String[] args) {

        if(HashFinder.findHash("Hello World!", "0")){
            System.out.println("Hash found!");
        }

    }
}

class HashFinder {

    public static boolean findHash(String data, String difficulty) {

        boolean isFound = false;
        long nonce = 0;

        do {
            // nonce zu übergebenem String konkatenieren
            String strToHash = data+nonce;
            // hash aus diesem String errechnen
            String hash = calculateHash(strToHash);
            // print found hashes
            // WARN! comment out for production
            System.out.printf("%s - %s - %s %n",nonce,Thread.currentThread().getName(),hash);

            if(hash.startsWith(difficulty)){
                isFound = true;
            }

            nonce++;
        } while (!isFound && nonce < Long.MAX_VALUE);

        return isFound;
    }

    /**
     * Calculates sha256 to a String
     * 
     * @param strToHash
     * @return
     */
    public static String calculateHash(String strToHash) {

        byte[] bytesOfMessage;
        try {
            bytesOfMessage = strToHash.getBytes("UTF-8");        
            MessageDigest md;        
            md = MessageDigest.getInstance("SHA-256");        
        
            byte[] thedigest = md.digest(bytesOfMessage);
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < thedigest.length; i++) {
                String hex = Integer.toHexString(0xff & thedigest[i]);
                if(hex.length() == 1) 
                        hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } 
        catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        
    }
}