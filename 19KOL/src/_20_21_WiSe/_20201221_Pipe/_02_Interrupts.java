package _20_21_WiSe._20201221_Pipe;

public class _02_Interrupts {
    public static void main(String[] args) {
        Runnable runner = () -> {

            //while (!Thread.interrupted()) {
            while (!Thread.currentThread().isInterrupted()) {
                System.out.println("Ich laufe immer noch");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    Thread.currentThread().interrupt();
                }
            }
            //Thread.currentThread().interrupt();
            System.out.println("Interrupted "+Thread.interrupted());

        };

        Thread t1 = new Thread(runner);
        t1.start();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        t1.interrupt();
    }
}


