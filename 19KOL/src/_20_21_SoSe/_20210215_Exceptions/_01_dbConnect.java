package _20_21_SoSe._20210215_Exceptions;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;

public class _01_dbConnect {

    public static void main(String[] args) {
        try {
            DbWork.connect();
        } catch (DatabaseNotAvailableException e) {
            e.printStackTrace();
        }

    }

}

class DbWork {

    public static void connect() throws DatabaseNotAvailableException {

        boolean connected = false;
        final int tries = 10;

        String url = "jdbc:mysql://localhost:3306/w3c";
        String user = "root";
        String password = "123";

        int counter = 0;
        while (!connected && counter < tries) {

            System.out.println("Trying to connect ...");

            try {
                Connection conn = DriverManager.getConnection(url, user, password);
                System.out.println("Connected!");
                // Blödsinn!
                // counter++;
                connected = true;
            } catch (SQLTimeoutException e) {
                System.out.println("Timeout occured!");
                // e.printStackTrace();
                connected = false;
            } catch (SQLException e) {
                System.out.println("Other SQL Exception");
                // e.printStackTrace();
                // nicht unbedingt notwendig:
                connected = false;
            }

            if(!connected){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }               

            counter++;
        }


        if(!connected) {
            throw new DatabaseNotAvailableException(counter);
        }

    }

}

class DatabaseNotAvailableException extends Exception {

    // für Serializale 
    private static final long serialVersionUID = 1L;

    DatabaseNotAvailableException(int tries) {
        super(String.format("Stopped connecting after %d tries. Zum testen: %5d %n",tries,123));
    }
    
} 
