package _20_21_SoSe._20210426_Concurrency_04;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class _01_Executor_example_simple {

    public static void main(String[] args) {

        System.out.println("Es laufen gerade Threads: " + Thread.activeCount());
        
        ExecutorService exec = Executors.newFixedThreadPool(5);

        //ExecutorService exec = Executors.newCachedThreadPool();

        System.out.println("Es laufen gerade Threads: " + Thread.activeCount());

        exec.execute(new Job(10));

        System.out.println("Es laufen gerade Threads: " + Thread.activeCount());

        Job run2 = new Job(10);

        int counter = 0;

        while(true){

            if(counter < 10) {
                System.out.println("Adding Job "+counter);
                exec.execute(run2);
            }

            System.out.println("Es laufen gerade Threads: " + Thread.activeCount());

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            counter++;

        }


    }
    
}

class Job implements Runnable {

    int runs;

    public Job(int runs) {
        this.runs = runs;
    }


    @Override
    public void run() {

        for (int i = 0; i < runs; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("Job done!");
        
    }

    
}
