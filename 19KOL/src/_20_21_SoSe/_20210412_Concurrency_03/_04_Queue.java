package _20_21_SoSe._20210412_Concurrency_03;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class _04_Queue {
    public static void main(String[] args) {

        DataShareQueue ds = new DataShareQueue();

        Runnable sender = () -> {
            for (int i = 0; i < 1000; i++) {
                ds.setDataString("Message: " + Integer.toString(i));
                try {
                    Thread.sleep(0);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        Runnable receiver = () -> {
            for (int i = 0; i < 1000; i++) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                ds.getDataString();
            }
        };

        new Thread(sender).start();
        new Thread(receiver).start();
        
    }
}

class DataShareQueue {

    private BlockingQueue<String> q = new ArrayBlockingQueue<>(5);

    public String getDataString(){        
        String returnVal="";
        try {
            returnVal = q.take();          
            System.out.println(returnVal); 
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
        return returnVal;
    }

    public void setDataString(String dataString){        
        try { 
            q.put(dataString);  
            System.out.println("Set: "+dataString);     
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}