package _20_21_SoSe._20210510_Concurrency_Docker;

public class _02_Dead_Lock_Synchronized {

    static Object lock1 = new Object();
	static Object lock2 = new Object();

	public static void main(String[] args) {
		
		Runnable run1 = () -> {
			while(true){
				
				synchronized(lock1){
					synchronized(lock2){
						System.out.printf("%s works!", Thread.currentThread().getName());
						
					}
				}
				
			}
		};

		Runnable run2 = () -> {
			while(true){
				
				synchronized(lock2){
					synchronized(lock1){
						System.out.printf("%s works!", Thread.currentThread().getName());						
					}
				}
				
			}
		};

		new Thread(run1,"run1").start();
		new Thread(run2,"run2").start();

	}

}
