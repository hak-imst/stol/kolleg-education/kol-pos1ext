package _21_22_WiSe._20211207_interrupt_hashfinder;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class _04_hash_finder {
    public static void main(String[] args) {
        String transactionData = "Liste über alle Transaktionen";
        // System.out.println(HashFinder.calculateHash(transactionData));
        HashFinder.findHash(transactionData, "000");
    }
}

class HashFinder {

    public static boolean findHash(String transactionData, String difficulty){
        boolean isFound = false;
        long nonce = 0;

        while(!isFound && nonce < Long.MAX_VALUE){
            String strToHash = transactionData+nonce;
            String hash = calculateHash(strToHash);
            // System.out.printf("%d - %s - %s %n",nonce,Thread.currentThread().getName(),hash);

            if(hash.startsWith(difficulty)){
                isFound = true;
                System.out.printf("%d - %s - %s %n",nonce,Thread.currentThread().getName(),hash);
            }
            nonce++;
        }

        return isFound;
    }

    /**
     * Calculates sha256 to a String
     * 
     * @param strToHash
     * @return
     */
    public static String calculateHash(String strToHash) {

        byte[] bytesOfMessage;
        try {
            bytesOfMessage = strToHash.getBytes("UTF-8");        
            MessageDigest md;        
            md = MessageDigest.getInstance("SHA-256");        
        
            byte[] thedigest = md.digest(bytesOfMessage);
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < thedigest.length; i++) {
                String hex = Integer.toHexString(0xff & thedigest[i]);
                if(hex.length() == 1) 
                        hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } 
        catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        
    }
}
