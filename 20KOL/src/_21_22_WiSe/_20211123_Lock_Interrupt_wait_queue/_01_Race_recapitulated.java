package _21_22_WiSe._20211123_Lock_Interrupt_wait_queue;

import java.util.LinkedList;

public class _01_Race_recapitulated {
    
    public static void main(String[] args) {        

        final int THREADS_N = 10;

        Runner run = new Runner();
        Thread thread = null;
        for(int i = 0; i < THREADS_N; i++){
            thread = new Thread(run);
            thread.start();
        }

        if(thread != null) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }        

        System.out.printf("Endstand Counter: %d %n", Storage.counter);

    }
}

class Runner implements Runnable {

    final int COUNT_TIMES = 1000;

    @Override
    public void run() {
        for(int i = 0; i < COUNT_TIMES; i++){
            count();
        }
    }

    synchronized void count(){
        Storage.counter ++;
    }
}

class Storage {
    public static long counter;
}
