package _21_22_WiSe._20211123_Lock_Interrupt_wait_queue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;


/**
 * Das folgende Programm verwendet für das Sender
 * Empfänger Problem nun eine Queue, was eine 
 * higher level Implementierung ist. Sie ist nicht
 * mehr so flexibel, wie wait() notify(), aber dafür
 * viel einfacher zu verwenden. Auch Fehler können
 * hier viel weniger leicht passieren. 
 * 
 * Dieses Programm sollte leicht zu verstehen sein. 
 * Der Queue können Daten hinzugefügt werden. Die
 * take() Methode liefert erst einen Wert zurück, 
 * wenn auch wieder einer vorhanden ist. 
 * 
 * Mögliches Anwendungsbeispiel: Ein Thread 1 holt 
 * Bilder von einer Webcam ab. Ein anderer Thread 2
 * bearbeitet sie nach. Thread 2 wartet immer, bis
 * ein neues Bild ankommt und bearbeitet es dann 
 * sofort. So kann diese Aufgabe parallel abgearbeitet
 * werden.
 * 
 */
public class _05_Queue {
    
    public static void main(String[] args) {

        DataShareQueue ds = new DataShareQueue();

        Runnable sender = () -> {
            for (int i = 0; i < 1000; i++) {
                ds.setDataString("Message: " + Integer.toString(i));
                try {
                    Thread.sleep(0);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        Runnable receiver = () -> {
            for (int i = 0; i < 1000; i++) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(ds.getDataString());
            }
        };

        new Thread(sender).start();
        new Thread(receiver).start();
        
    }
}

class DataShareQueue {

    private BlockingQueue<String> q = new ArrayBlockingQueue<>(5);

    public String getDataString(){        
        String returnVal="";
        try {
            returnVal = q.take(); 
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
        return returnVal;
    }

    public void setDataString(String dataString){        
        try { 
            q.put(dataString);  
            System.out.println("Set: "+dataString);     
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
